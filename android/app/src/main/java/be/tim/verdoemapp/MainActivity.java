package be.tim.verdoemapp;

import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;

// TODO:
// - ~~Notification (blocking with stop action)~~
// - Settings: - 0-20 alarm sound
//             - audio file chooser
//             - match number field(s) (contact picker)
//             - match sms contents field
// - Check and ask for sms permissions
// - ~~Start on boot service~~ (= handled by broadcast receiver!)
// - Add (temp) disable option
public class MainActivity extends FlutterActivity {
  public static final String MY_PREFS_NAME = "FlutterSharedPreferences";
  public static final String PREF_KEY_PHONE_NR = "flutter.PREF_KEY_PHONE_NR";
  public static final String PREF_KEY_VOLUME = "flutter.PREF_KEY_VOLUME";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);
  }
}
