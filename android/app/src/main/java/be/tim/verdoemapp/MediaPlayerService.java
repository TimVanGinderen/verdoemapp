package be.tim.verdoemapp;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;

public class MediaPlayerService extends Service implements MediaPlayer.OnPreparedListener {
    private static final String TAG = "MediaPlayerService";

    public static final String ACTION_PLAY = "be.tim.action.PLAY";
    public static final String ACTION_STOP = "be.tim.action.STOP";

    private static final int DEFAULT_VOLUME = 5; // 0-20

    MediaPlayer mediaPlayer = null;

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG,"onStartCommand start" );
        if (intent.getAction().equals(ACTION_PLAY)) {
            // init media-player
            mediaPlayer = new MediaPlayer();

            // load mp3 asset
            try {
                AssetFileDescriptor descriptor = getAssets().openFd("alongcomesmary.mp3");
                mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                descriptor.close();
            } catch(IOException e) {
                Log.e(TAG, "Exception while trying to open sound asset", e);
            }

            // Get volume from shared prefs
            SharedPreferences prefs = getSharedPreferences(MainActivity.MY_PREFS_NAME, MODE_PRIVATE);
//            int volume = prefs.getInt(MainActivity.PREF_KEY_VOLUME, Integer.MIN_VALUE);
            int volume = (int) prefs.getLong(MainActivity.PREF_KEY_VOLUME, Integer.MIN_VALUE);

            if (volume == Integer.MIN_VALUE) {
                volume = DEFAULT_VOLUME;
            }

            Log.i(TAG, "Volume = " + volume);

            // set volume
            AudioManager audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);

            // set listener
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.prepareAsync(); // prepare async to not block main thread
        } else if (intent.getAction().equals(ACTION_STOP)) {
            // stop media playback
            if (mediaPlayer != null) {
                mediaPlayer.stop();
            }

            // hide notification
            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(MessageReceiver.NOTIFICATION_ID);
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /** Called when MediaPlayer is ready */
    public void onPrepared(MediaPlayer player) {
        player.start();
    }
}