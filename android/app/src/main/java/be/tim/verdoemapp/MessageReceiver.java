package be.tim.verdoemapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;

public class MessageReceiver extends BroadcastReceiver {

    private static final String TAG = "MessageReceiver";

    private static final String CHANNEL_ID = "be.tim.godverdoemapp.notify_001";
    public static final int NOTIFICATION_ID = 1;

    public void onReceive(Context context, Intent intent) {
        Bundle pudsBundle = intent.getExtras();
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
        Log.i(TAG, messages.getMessageBody());
        Log.i(TAG, messages.getOriginatingAddress());

        // Get phone number from shared prefs
        SharedPreferences prefs = context.getSharedPreferences(MainActivity.MY_PREFS_NAME, context.MODE_PRIVATE);
        String phoneNumber = prefs.getString(MainActivity.PREF_KEY_PHONE_NR, null);

        if (phoneNumber == null) {
//            return;
            phoneNumber = "+32475932921";
        }

        // Check if message nr matches the one from shared prefs
        if (messages.getOriginatingAddress().contains(phoneNumber)) {
            Log.i(TAG, "Contains " + phoneNumber);
            showNotification(context);
            playSound(context);
        }
    }

    private void playSound(Context context) {
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(MediaPlayerService.ACTION_PLAY);
        context.startService(intent);
    }

    private void showNotification(Context context) {
        Intent intent = new Intent(context, MediaPlayerService.class);
        intent.setAction(MediaPlayerService.ACTION_STOP);
        int requestID = (int) System.currentTimeMillis();

        PendingIntent pendingIntent = PendingIntent.getService(context, requestID, intent, 0);

        // Build notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText("Zet die ploat af")
                .setAutoCancel(false)
                .addAction(R.drawable.ic_launcher_background, context.getString(R.string.action_stop),
                        pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX);


        // Issue the notification
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(CHANNEL_ID);
        }

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;

        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}
