import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:shared_preferences/shared_preferences.dart';

// TODO:
// - ~~Notification (blocking with stop action)~~
// - Settings: - 0-20 alarm sound
//             - audio file chooser
//             - match number field(s) (contact picker)
//             - match sms contents field
// - Check and ask for sms permissions
// - ~~Start on boot service~~ (= handled by broadcast receiver!)
// - Add (temp) disable option

//ok message = keyword(user input) in text messagge, and don't play sound option
//repeating sounds (text message in text to speech also)
//enablen disablen
//led flashen
//trillen

void main() {
  // Enable integration testing with the Flutter Driver extension.
  // See https://flutter.io/testing/ for more info.
  enableFlutterDriverExtension();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: MyHomePage(title: 'Dit is het mooiste'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _selectedValue = 5.0;

  void _saveVolume(int volume) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('PREF_KEY_VOLUME', volume);
  }

  void _savePhoneNumber(String number) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('PREF_KEY_PHONE_NR', number);
  }

  @override
  Widget build(BuildContext context) {
    final numberController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TextField(
//              controller: numberController,
//              decoration: InputDecoration(
//                  labelText: 'Nummer:', hintText: '+32478321432'),
//              onChanged: (String value) => _savePhoneNumber(value),
                ),
            ListTile(
              title: Text('Volume:'),
              subtitle: Slider(
                min: 0,
                max: 20,
                value: _selectedValue.toInt().toDouble(),
                divisions: 21,
                onChanged: (double value) {
                  print('$value | ${value.toInt()}');
                  _saveVolume(value.toInt());
                  setState(() {
                    _selectedValue = value;
                  });
                },
              ),
              trailing: Text('${_selectedValue.toInt()}'),
            ),
          ],
        ),
      ),
    );
  }
}
